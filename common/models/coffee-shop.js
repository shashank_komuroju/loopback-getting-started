module.exports = function (Coffeeshop) {

  Coffeeshop.status = function (hour, cb) {
    var currentHour = (new Date()).getHours();

    try {
      if (!isNaN(hour)) {
        currentHour = hour;
      }
    }
    catch (ex) {
      console.log("Wrong argument or argument isn't entered : " + hour);
      console.log("The exception : " + ex);
    }

    var OPEN_HOUR = 9;
    var CLOSE_HOUR = 21;
    var response;

    console.log("The hour is : " + currentHour);

    if (currentHour >= OPEN_HOUR && currentHour <= CLOSE_HOUR) {
      response = "We are open now. Come have a cup of Coffee ;) "
    }
    else {
      response = "Sorry, ): We are closed now. Working hours from 9am to 9pm only.";
    }
    return cb(null, response);
  };

  Coffeeshop.remoteMethod(
    'status',
    {
      http: {path: '/status', verb: 'get'},
      accepts: {arg: 'hour', type: 'number', required: false},
      returns: {type: 'string', arg: 'status'}
    }
  )

};
